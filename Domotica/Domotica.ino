#include <dht11.h>
#include <Ethernet.h>
#include <SPI.h>
#include <WebServer.h>

byte mac[] = { 
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress ip(192,168,1, 177);
WebServer webserver("", 80);


#define DHT11PIN_int 2
#define DHT11PIN_est 3

dht11 DH11_INT;
dht11 DH11_EST;
int TV = 9;
int Mobile = 8;
boolean ledTV=false, ledMob=false;

//String h1="<font color=\"#663366\" size=\"7\" align=\"left\">" , h3="<font color=\"#00CCFF\" size=\"5\" align=\"left\">";


void Start(WebServer &server, WebServer::ConnectionType type,
char *url_param, bool param_complete)
{
  //restituisce al browser l'intestazione http 200 OK
  server.httpSuccess();


  //gestisco il tipo di richiesta HEAD
  if (type != WebServer::HEAD)
  {
    String s = "";

    if (param_complete == true)
    {
      s = url_param;
      
      if (s == "ledTV=ON") //Accendi TV
      {
        ledTV=true;
        digitalWrite(TV, HIGH);
      }

      else if (s == "ledTV=OFF") //Segni TV
      {
        ledTV=false;
        digitalWrite(TV, LOW);
      }
      else if (s == "ledMob=ON") //Accendi Mobile
      {
        ledMob=true;
        digitalWrite(Mobile, HIGH);
      }

      else if (s == "ledMob=OFF") //Spegni Mobile
      {
        ledMob=false;
        digitalWrite(Mobile, LOW);
      }
      
      else if(s == "stato")
      {
        DH11_INT.read(DHT11PIN_int);
        DH11_EST.read(DHT11PIN_est);
        server.print("{\"conf\":[{\"ledTV\":\""+(String)ledTV+"\",\"ledMob\":\""+(String)ledMob
                      +"\",\"tempInt\":\""+(String)DH11_INT.temperature+"\",\"tempEst\":\""+(String)DH11_EST.temperature
                      +"\",\"UmInt\":\""+(String)DH11_INT.humidity+"\",\"UmEst\":\""+(String)DH11_EST.humidity+"\"}]}");
      } 
        /*
      else if (s=="h"){
        //gestisco la pagina html in base allo stato delle uscite di Arduino
        P(htmlHead) =
          "<meta http-equiv=\"refresh\" content=\"10;url=\"/>"
          "<html>"
          "<head>"
          "<title>Domotica</title>"
          "</head>"
          "<body background=\"http://bit.ly/YxfGJA\">";

        server.printP(htmlHead);
        server.print("<iframe src=\"http://bit.ly/Xq00t4\" frameborder=\"0\"  width=\"100%\"></iframe>");
        
        DH11_INT.read(DHT11PIN_int);
        DH11_EST.read(DHT11PIN_est);
        server.print(h1+"Interno:</font><br>"+h3+"Umidità (%): ");
        server.print((float)DH11_INT.humidity);
        server.print("<br>Temperatura: ");
        server.print((float)DH11_INT.temperature);
        server.print(" °C</font><br><br><br>"+h1+"Esterno:</font><br>"+h3+"Umidità (%): ");
        server.print((float)DH11_EST.humidity);
        server.print("<br>Temperatura: ");
        server.print((float)DH11_EST.temperature);
        server.print(" °C</font></body></html>");

      }*/
      
    }
  }
}
//=============================================================================


void setup()
{
  pinMode(TV, OUTPUT);   
  pinMode(Mobile, OUTPUT);   
  digitalWrite(TV, LOW);
  digitalWrite(Mobile, LOW);
  Serial.begin(9600);
  
  Ethernet.begin(mac, ip);
  webserver.setDefaultCommand(&Start);
  webserver.addCommand("index.htm", &Start);

  webserver.begin();

  delay(100);
  
}

void loop()
{  
   webserver.processConnection();
}

